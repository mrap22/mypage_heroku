from django.db import models


class Jadwal(models.Model):
    nama = models.CharField(max_length=30, unique = True, default='')
    waktu = models.DateTimeField()
    tempat = models.CharField(max_length=30, default='')
    kategori = models.CharField(max_length=30, default='')

    def __str__(self):
        return self.nama
