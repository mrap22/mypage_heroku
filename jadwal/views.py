from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django import template

from .forms import FormJadwal
from .models import Jadwal
from django.utils import timezone

import datetime

def index(request):
    text = ''
    if request.method == "POST":
        form = FormJadwal(request.POST)
        if form.is_valid():
            temp = form.save(commit=False)
            temp.waktu = timezone.now()
            temp.save_m2m()

            for i in Jadwal.objects.all():
                text = text + '> '+ i.nama + '\n- ' + i.waktu.strftime("%d-%m-%Y %H:%M") + '\n-' + i.tempat + '\n-' + i.kategori
                print(text)
            return HttpResponseRedirect(request.path, {'txt' : text})

    else:
        if(request.GET.get('deletbut')):
            #for i in Jadwal.objects.all():
            #    i.row.remove()
            Jadwal.delete()

        form = FormJadwal()
        for i in Jadwal.objects.all():
            text = text + '> '+ i.nama + '\n- ' + i.waktu.strftime("%d-%m-%Y %H:%M") + '\n-' + i.tempat + '\n-' + i.kategori
            print(text)

    context = {'form': form, 'txt' : text,}

    return render(request, 'jadwal/jadwal.html', context)
