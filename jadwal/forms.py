from django import forms
from django.forms import ModelForm

from .models import Jadwal

class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime'

class FormJadwal(ModelForm):
    class Meta:
        model = Jadwal
        fields = ['nama', 'waktu', 'tempat', 'kategori',]
        widgets = {
            'waktu': DateTimeInput()
        }


        # pull dari git kalo rusak lmao
